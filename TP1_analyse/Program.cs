﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1_analyse
{
    class Program
    {
        static void Main(string[] args)
        {
            //instanciation
            Gestion gestionnaire = new Gestion();
            //utilisation des différents blocs
            gestionnaire.Saisie();
            
            gestionnaire.AffichageBCD(gestionnaire.ConversionBCD(gestionnaire.Nombre));

            Console.Read(); //pour que la console ne se ferme pas
        }
    }
}
