﻿using System;

namespace TP1_analyse
{
    class Gestion
    {
        int nombre = -1;

        public int Nombre { get => nombre; }

        public void Saisie()
        {
            //saisie du nombre
            while (Nombre < 0 || Nombre > int.MaxValue)
            {
                try
                {
                    Console.WriteLine("saisissez le nombre (entre 0 et " + int.MaxValue + ") :");
                    nombre = int.Parse(Console.ReadLine());
                }
                catch (Exception e)
                {
                    //si erreur de saisie / conversion --> affichage de l'erreur
                    Console.Clear();
                    Console.WriteLine(e.Message);
                }
            }
        }
        public bool[,] ConversionBCD(int nombreEntre)
        {
            string nombre = nombreEntre.ToString();
            int NbChiffres = nombre.Length;

            if (NbChiffres<=10) //on DOIT avoir maximum 10 chiffres, sinon erreur
            {
                //tableau qui contient le BCD de chaque nombre
                bool[,] BCD = new bool[4,NbChiffres];
                for (int i = 0; i < NbChiffres; i++)
                {
                    switch (int.Parse(nombre[i].ToString()))    //conversion de chaque caractère en nombre.
                    {
                        case 0:
                            BCD[0, i] = false;
                            BCD[1, i] = false;
                            BCD[2, i] = false;
                            BCD[3, i] = false;
                            break;
                        case 1:
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            break;
                        case 2:
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            BCD[0, i] = false;
                            break;
                        case 3:
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            BCD[0, i] = true;
                            break;
                        case 4:
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            break;
                        case 5:
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            break;
                        case 6:
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            BCD[0, i] = true;
                            BCD[0, i] = false;
                            break;
                        case 7:
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            BCD[0, i] = true;
                            BCD[0, i] = true;
                            break;
                        case 8:
                            BCD[0, i] = true;
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            break;
                        case 9:
                            BCD[0, i] = true;
                            BCD[0, i] = false;
                            BCD[0, i] = false;
                            BCD[0, i] = true;
                            break;
                        default:
                            Console.WriteLine("erreur de conversion");
                            break;
                    }
                }

                return BCD;
            }
            else
            {
                //levé d'une exeption
                Console.Clear();
                throw new ArgumentException("Nombre de chiffre supérieur a 10");
            }
        }
        public void AffichageBCD(bool[,] BCD)
        {
            //affichage du BCD
            for (int chiffre = 0; chiffre < BCD.Length/4; chiffre++)
            {
                for (int binaire = 0; binaire < 4; binaire++)
                {
                    Console.Write(BCD[binaire, chiffre] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
